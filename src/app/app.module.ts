import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { MenuComponent } from "./components/menu/menu.component";
import { CartComponent } from "./components/cart/cart.component";
import { FoodComponent } from "./components/food/food.component";
import { FormSearchComponent } from "./components/form-search/form-search.component";

import { ProductService } from "./services/product.service";

import { routing } from "./app.routing";

@NgModule({
  declarations: [
    AppComponent,
    CartComponent,
    FoodComponent,
    MenuComponent,
    FormSearchComponent
  ],
  imports: [BrowserModule, routing],
  providers: [ProductService],
  bootstrap: [AppComponent]
})
export class AppModule {}
