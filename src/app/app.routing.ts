import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { CartComponent } from "./components/cart/cart.component";
import { FoodComponent } from "./components/food/food.component";

const routes: Routes = [
  { path: "", component: FoodComponent },
  { path: "products", component: FoodComponent },
  { path: "cart", component: CartComponent },
  { path: "**", redirectTo: "" }
];

export const routing = RouterModule.forRoot(routes);
