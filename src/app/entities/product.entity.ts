export class Product {
  id: string;
  title: string;
  price: number;
  cuisine: string;
  photoPath?: string;
}
