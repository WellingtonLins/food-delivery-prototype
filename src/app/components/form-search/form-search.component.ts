import { Component, OnInit } from "@angular/core";
import { Observable, Subject } from "rxjs";

import { debounceTime, distinctUntilChanged, switchMap } from "rxjs/operators";

import { Product } from "../../entities/product.entity";
import { ProductService } from "../../services/product.service";

@Component({
  selector: "app-form-search",
  templateUrl: "./form-search.component.html",
  styleUrls: ["./form-search.component.css"]
})
export class FormSearchComponent implements OnInit {
  ngOnInit() {}
}
