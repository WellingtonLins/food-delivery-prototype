import { Component, OnInit } from "@angular/core";

import { Product } from "../../entities/product.entity";
import { ProductService } from "../../services/product.service";

@Component({
  selector: "app-food",
  templateUrl: "./food.component.html",
  styleUrls: ["./food.component.css"]
})
export class FoodComponent implements OnInit {
  products: Product[];

  constructor(private productService: ProductService) {}

  ngOnInit() {
    this.products = this.productService.findAll();
  }
}
